##
## Makefile for my_printf in /home/kirsz_m/rendu/PSU_2015_my_printf
## 
## Made by Maxime Kirsz
## Login   <kirsz_m@epitech.net>
## 
## Started on  Wed Nov  4 14:09:08 2015 Maxime Kirsz
## Last update Fri Nov 27 23:53:11 2015 Maxime Kirsz
##

RM	= rm -rf

CC	= gcc

NAME	= libmy.a

SRC	= src/my_printf.c \
	src/my_putunbr_base.c \
	src/str.c \
	src/tools.c \
	src/toolsbis.c \
	src/toolster.c \
	src/my_putnbr.c \
	src/my_put_pointer.c

OBJ	= $(SRC:.c=.o)

AC	= ar rc

RL	= ranlib


all: $(NAME)

$(NAME): $(OBJ)
	$(AC) $(NAME) $(OBJ)
	$(RL) $(NAME)

clean:
	$(RM) $(OBJ)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
