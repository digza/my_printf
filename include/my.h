/*
** my.h for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Nov  4 17:26:41 2015 Maxime Kirsz
** Last update Sun Nov 15 18:01:47 2015 Maxime Kirsz
*/

#ifndef MY_H_
# define MY_H_

#include <stdarg.h>

typedef struct	s_flags
{
  int		ret;
  char		flag;
  int		(*ptr)();
}		t_flags;

/*
** my_.c
*/

int	my_putchar(char, t_flags*);
void	my_putstr(char*, t_flags*);
int	my_strlen(char*);
int	my_putnbr(int, t_flags*);
void	my_putnbr_base(int, char*, t_flags*);
int	my_putunbr_base(unsigned int, char*, t_flags*);
int	my_put_pointer(unsigned long long, char*, t_flags*);
/*
** tools.c
*/

int	character(va_list, t_flags*);
int	string(va_list, t_flags*);
int	number(va_list, t_flags*);
int	hexalower(va_list, t_flags*);
int	binary(va_list, t_flags*);
/*
** toolsbis.c
*/

int	hexaupper(va_list, t_flags*);
int	octal(va_list, t_flags*);
int	address(va_list, t_flags*);
int	unumber(va_list, t_flags*);
int	noflag(t_flags*);
/*
** toolster.c
*/

int	nprint(va_list, t_flags*);
void	nprint_special(char*, int, int, t_flags*);
/*
** my_printf.c
*/

void	fill_struct(t_flags*);
int	my_printf(char*, ...);
int	browse_functions(char*, int, va_list, t_flags*);

#endif /* !MY_H_ */
