/*
** my_printf.c for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Nov  4 17:26:26 2015 Maxime Kirsz
** Last update Fri Nov 27 23:49:30 2015 Maxime Kirsz
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include "../include/my.h"

void	fill_struct(t_flags *flags)
{
  flags[0].ret = 0;
  flags[0].flag = 'i';
  flags[0].ptr = &number;
  flags[1].flag = 'd';
  flags[1].ptr = &number;
  flags[2].flag = 'u';
  flags[2].ptr = &unumber;
  flags[3].flag = 'o';
  flags[3].ptr = &octal;
  flags[4].flag = 'x';
  flags[4].ptr = &hexalower;
  flags[5].flag = 'X';
  flags[5].ptr = &hexaupper;
  flags[6].flag = 'c';
  flags[6].ptr = &character;
  flags[7].flag = 's';
  flags[7].ptr = &string;
  flags[8].flag = 'S';
  flags[8].ptr = &nprint;
  flags[9].flag = 'p';
  flags[9].ptr = &address;
  flags[10].flag = 'b';
  flags[10].ptr = &binary;
  flags[11].flag = '%';
  flags[11].ptr = &noflag;
}

int	browse_functions(char *format, int i, va_list ap, t_flags *flags)
{
  int	j;

  j = 0;
  i++;
  while (format[i] != flags[j].flag && j < 12)
    j++;
  if (j > 11)
    {
      my_putchar('%', flags);
      my_putchar(format[i], flags);
    }
  else
    flags[j].ptr(ap, flags);
  i++;
  return (i);
}

int		my_printf(char *format, ...)
{
  va_list	ap;
  t_flags	*flags;
  int		i;
  int		ret;

  if ((flags = malloc(sizeof(*flags) * 12)) == NULL)
    return (0);
  fill_struct(flags);
  i = 0;
  va_start(ap, format);
  while (format[i] != '\0')
    {
      while (format[i] != '%' && format[i] != '\0')
	my_putchar(format[i++], flags);
      if (format[i] == '%' && format[i + 1] != '\0')
	i = browse_functions(format, i, ap, flags);
      else if (format[i] == '%' && format[i + 1] == '\0')
	  i++;
    }
  va_end(ap);
  ret = flags[0].ret;
  free (flags);
  return (ret);
}
