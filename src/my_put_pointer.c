/*
** my_put_pointer.c for my_printf in /home/digza/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Nov 11 10:40:02 2015 Maxime Kirsz
** Last update Fri Nov 27 23:51:53 2015 Maxime Kirsz
*/

#include <unistd.h>
#include "../include/my.h"

int	my_put_pointer(unsigned long long nb, char *base, t_flags *flags)
{
  long long	len;
  long long	div;
  long long	index;

  div = 1;
  len = 0;
  len = my_strlen(base);
  while (nb / div >= len)
    div = div * len;
  while (div >= 1)
    {
      index = nb /div;
      my_putchar(base[index], flags);
      nb = nb - (index * div);
      div = div / len;
    }
  return (0);
}
