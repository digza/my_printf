/*
** my_putnbr.c for my_putnbr in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Nov  4 18:15:51 2015 Maxime Kirsz
** Last update Fri Nov 27 23:51:36 2015 Maxime Kirsz
*/

#include <unistd.h>
#include "../include/my.h"

int	my_putnbr(int nb, t_flags *flags)
{
  int	div;

  div = 1;
  if (nb == -2147483648)
    {
      my_putstr("-2147483648", flags);
      return (0);
    }
  if (nb < 0)
    {
      my_putchar('-', flags);
      nb = -nb;
    }
  while (nb / div >= 10)
    div = div * 10;
  while (div >= 1)
    {
      my_putchar(((nb / div) % 10) + '0', flags);
      div = div / 10;
    }
  return (0);
}
