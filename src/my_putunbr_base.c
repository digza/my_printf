/*
** my_putunbr_base.c for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Thu Nov  5 16:16:06 2015 Maxime Kirsz
** Last update Fri Nov 27 23:52:05 2015 Maxime Kirsz
*/

#include <unistd.h>
#include "../include/my.h"

int	my_putunbr_base(unsigned int nb, char *base, t_flags *flags)
{
  unsigned int	len;
  unsigned int	div;
  unsigned int	index;

  div = 1;
  len = 0;
  len = my_strlen(base);
  while (nb / div >= len)
    div = div * len;
  while (div >= 1)
    {
      index = nb / div;
      my_putchar(base[index], flags);
      nb = nb - (index * div);
      div = div / len;
    }
  return (0);
}
