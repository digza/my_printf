/*
** str.c for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/src
** 
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
** 
** Started on  Fri Nov 27 23:51:25 2015 Maxime Kirsz
** Last update Fri Nov 27 23:51:27 2015 Maxime Kirsz
*/

#include <unistd.h>
#include "../include/my.h"

int	my_putchar(char c, t_flags *flags)
{
  write(1, &c, 1);
  flags[0].ret++;
  return (0);
}

void	my_putstr(char *str, t_flags *flags)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i], flags);
      i = i + 1;
    }
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i = i + 1;
    }
  return (i);
}
