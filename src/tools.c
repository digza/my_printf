/*
** tools.c for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Nov  4 22:16:53 2015 Maxime Kirsz
** Last update Fri Nov 27 23:52:17 2015 Maxime Kirsz
*/

#include <stdlib.h>
#include <stdarg.h>
#include "../include/my.h"

int		character(va_list ap, t_flags *flags)
{
  char		data;

  data = va_arg(ap, int);
  my_putchar(data, flags);
  return (0);
}

int		string(va_list ap, t_flags *flags)
{
  char		*data;

  data = va_arg(ap, char*);
  if (data == NULL)
    {
      my_putstr("(null)", flags);
      return (0);
    }
  my_putstr(data, flags);
  return (0);
}

int		number(va_list ap, t_flags *flags)
{
  int		data;

  data = va_arg(ap, int);
  my_putnbr(data, flags);
  return (0);
}

int		binary(va_list ap, t_flags *flags)
{
  unsigned int	data;

  data = va_arg(ap, unsigned int);
  my_putunbr_base(data, "01", flags);
  return (0);
}

int		hexaupper(va_list ap, t_flags *flags)
{
  unsigned int	data;

  data = va_arg(ap, unsigned int);
  my_putunbr_base(data, "0123456789ABCDEF", flags);
  return (0);
}
