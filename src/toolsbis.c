/*
** toolsbis.c for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Wed Nov  4 22:24:00 2015 Maxime Kirsz
** Last update Fri Nov 27 23:52:26 2015 Maxime Kirsz
*/

#include <stdlib.h>
#include <stdarg.h>
#include "../include/my.h"

int		octal(va_list ap, t_flags *flags)
{
  unsigned int	data;

  data = va_arg(ap, unsigned int);
  my_putunbr_base(data, "01234567", flags);
  return (0);
}

int		address(va_list ap, t_flags *flags)
{
  long long	data;

  data = va_arg(ap, long long);
  if (data == 0)
    {
      my_putstr("(nil)", flags);
      return (0);
    }
  my_putstr("0x", flags);
  my_put_pointer(data, "0123456789abcdef", flags);
  return (0);
}

int		hexalower(va_list ap, t_flags *flags)
{
  unsigned int	data;

  data = va_arg(ap, unsigned int);
  my_putunbr_base(data, "0123456789abcdef", flags);
  return (0);
}

int		unumber(va_list ap, t_flags *flags)
{
  unsigned int	data;

  data = va_arg(ap, unsigned int);
  my_putunbr_base(data, "0123456789", flags);
  return (0);
}

int		noflag(t_flags *flags)
{
  my_putchar('%', flags);
  return (0);
}
