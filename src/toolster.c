/*
** toolster.c for my_printf in /home/kirsz_m/rendu/PSU/PSU_2015_my_printf/lib/my
**
** Made by Maxime Kirsz
** Login   <kirsz_m@epitech.net>
**
** Started on  Thu Nov  5 10:56:19 2015 Maxime Kirsz
** Last update Fri Nov 27 23:52:37 2015 Maxime Kirsz
*/

#include <stdlib.h>
#include <stdarg.h>
#include "../include/my.h"

int	nprint(va_list ap, t_flags *flags)
{
  int	i;
  int	value;
  char	*data;

  i = 0;
  data = va_arg(ap, char*);
  if (data == NULL)
    {
      my_putstr("(null)", flags);
      return (0);
    }
  while (data[i] && (data[i] >= 32 || data[i] <= 127))
    {
      if ((data[i] < 32 || data[i] == 127) && data[i] != 0)
	{
	  value = 0;
	  while (data[i] != value)
	    value++;
	  nprint_special(data, i, value, flags);
	  i++;
	}
      else
	my_putchar(data[i++], flags);
    }
  return (0);
}

void	nprint_special(char *data, int i, int value, t_flags *flags)
{
  if (value < 10)
    {
      my_putstr("\\00", flags);
      my_putnbr(value, flags);
    }
  else if (value >= 10 && value <= 31)
    {
      my_putstr("\\0", flags);
      my_putnbr(value, flags);
    }
  else
    {
      my_putstr("\\", flags);
      my_putunbr_base(value, "01234567", flags);
    }
}
